{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module Main where

import Web.Scotty
import Data.Aeson (FromJSON, ToJSON)
import GHC.Generics
import Network.Wai.Middleware.Static
import System.Random
import System.IO.Unsafe (unsafePerformIO)

import Service.Game
import Genetic.Algorithm
import Service.Entities

main = do
    putStrLn "Starting Server..."
    scotty 3000 $ do
      middleware $ staticPolicy (noDots >-> addBase "site")
      get "/" $ file "./site/index.html"
      get "/decideAbilities" $ do
        json getAllDecideAbilityWrapper
      get "/abilities" $ do
        json getAllAbilityWrapper
      post "/fightToDeath" $ do
        wrappedInitialState <- jsonData :: ActionM FightStateWrapper
        let (player1, player2in, r) = unwrapFightState wrappedInitialState
        let player2 = if (getLife player2in == -1) then ((unsafePerformIO (getTrainedCharacter r))) else player2in
        let wrappedFight = (wrapFightResult . fight) (player1, player2)
        json wrappedFight
      post "/initFight" $ do
        wrappedInitialState <- jsonData :: ActionM FightStateWrapper
        let (player1, player2in, r) = unwrapFightState wrappedInitialState
        let player2 = if (getLife player2in == -1) then ((unsafePerformIO (getTrainedCharacter r))) else player2in
        let wrappedState= wrapFightState  (player1, player2, 0)
        json wrappedState
      post "/fightNextStage" $ do
        wrappedCurrentState <- jsonData :: ActionM FightStateWrapper
        let currentState = unwrapFightState wrappedCurrentState
        let wrappedState = wrapFightState (fightOneStep currentState)
        json wrappedState