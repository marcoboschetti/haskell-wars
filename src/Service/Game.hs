{-# LANGUAGE DeriveGeneric #-}

module Service.Game where

import Data.Aeson (FromJSON, ToJSON)
import GHC.Generics
import Network.Wai.Middleware.Static
import Service.Entities

--------- Service -------------
unwrapPlayer :: PlayerWrapper -> Player
unwrapPlayer (PlayerWrapper l m i a h1 h2) = Character l m  (unwrapItems i) (unwrapDecideAbilityStruct a) (unwrapAbilityStruct h1) (unwrapAbilityStruct h2)

-------------------------------------------------------------
----------------- CHARACTER MARSHALLING ---------------------
-------------------------------------------------------------

data PlayerWrapper = PlayerWrapper {
  life :: Int,
  mana :: Int,
  items :: [ItemWrapper],
  decideAbilityIndex :: Int,
  ability1Index :: Int,
  ability2Index :: Int 
  } deriving (Generic, Show)

instance FromJSON PlayerWrapper
instance ToJSON PlayerWrapper

------------------- ITEMS -----------------
data ItemWrapper = ItemWrapper{
  element :: String,
  itemType :: String
  } deriving (Generic, Show)

instance FromJSON ItemWrapper
instance ToJSON ItemWrapper

wrapItems :: [Item] -> [ItemWrapper]
wrapItems = map (\(Item elem itemType) -> ItemWrapper (show elem) (show itemType))

unwrapItems :: [ItemWrapper] -> [Item]
unwrapItems = map (\(ItemWrapper elem itemType) -> Item (elementFromString elem) (itemTypeFromString itemType))

elementFromString text = head $ filter (\elem -> show elem == text) [Fire, Rock, Air, Earth, Water]
itemTypeFromString text = head $ filter (\item -> show item == text) [ Staff, Boots, Helmet, Chestplate, Pants]


data FightStateWrapper = FightStateWrapper{
  player1 :: PlayerWrapper,
  player2 :: PlayerWrapper,
  round :: Int
  } deriving (Generic, Show)

instance FromJSON FightStateWrapper
instance ToJSON FightStateWrapper

wrapPlayer :: Player -> PlayerWrapper
wrapPlayer (Character l1 m1 i1 a1 h11 h12) = PlayerWrapper l1 m1 (wrapItems i1) (wrapDecideAbilityStruct a1) (wrapAbility h11) (wrapAbility h12)  --  TODO: Encode items and funcion indexes [] a1 h11 h12

wrapAbility :: AbilityStruct -> Int
wrapAbility (AbilityStruct name func index mana) = index

wrapDecideAbilityStruct :: DecideAbilityStruct -> Int
wrapDecideAbilityStruct (DecideAbilityStruct name da index) = index 

unwrapDecideAbilityStruct :: Int -> DecideAbilityStruct
unwrapDecideAbilityStruct index = head $ filter (\(DecideAbilityStruct d f i) -> i == index) getAllDecideAbilityRaw

unwrapAbilityStruct :: Int -> AbilityStruct
unwrapAbilityStruct index = head $ filter (\(AbilityStruct d f i m) -> i == index) getAllAttacksRaw

wrapFightResult :: [FightState] -> [FightStateWrapper]
wrapFightResult = map wrapFightState

wrapFightState :: FightState -> FightStateWrapper
wrapFightState (c1,c2,n) = FightStateWrapper (wrapPlayer c1) (wrapPlayer c2) n

unwrapFightState ::  FightStateWrapper -> FightState
unwrapFightState  (FightStateWrapper wc1 wc2 n) = ((unwrapPlayer wc1),(unwrapPlayer wc2),n)

--------------- DECIDE ABILITY ---------
data DecideAbilityWrapper = DecideAbilityWrapper{
  daid :: Int,
  daname :: String
} deriving (Generic, Show)

instance FromJSON DecideAbilityWrapper
instance ToJSON DecideAbilityWrapper

getAllDecideAbilityWrapper :: [DecideAbilityWrapper]
getAllDecideAbilityWrapper = map (\(DecideAbilityStruct desc f index) -> DecideAbilityWrapper index desc) getAllDecideAbilityRaw

-- Add each new productive ability here
getAllDecideAbilityRaw :: [DecideAbilityStruct]
getAllDecideAbilityRaw = [decideAttacksManuallyStruct, decideFirstAbilityStruct, decideAlterateAbilityStruct, decideForcedFirstAbilityStruct, decideForcedSecondAbilityStruct]

--------------- ABILITY ---------
data AbilityWrapper = AbilityWrapper{
  aid :: Int,
  aname :: String,
  amana :: Int
} deriving (Generic, Show)

instance FromJSON AbilityWrapper
instance ToJSON AbilityWrapper

getAllAbilityWrapper :: [AbilityWrapper]
getAllAbilityWrapper = map (\(AbilityStruct desc f index mana) -> AbilityWrapper index desc mana) getAllAttacksRaw

-- Add each new productive ability here
getAllAttacksRaw :: [AbilityStruct]
getAllAttacksRaw = [simpleHitStruct,setOnFireStruct,nullHitStruct, manaLeechStruct, lifeStealStruct, generateManaStruct, windHitStruct]