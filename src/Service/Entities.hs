{-# LANGUAGE DeriveGeneric #-}

module Service.Entities where

import GHC.Generics
import Network.Wai.Middleware.Static

-------------------------------------------------------------
----------------- CHARACTER ---------------------------------
-------------------------------------------------------------

-- AbiltyStruct name func index mana 
data AbilityStruct = AbilityStruct String Ability Int Int
type Ability = ((Player, Player) -> (Player, Player))

data Item = Item Element ItemType deriving Eq
data Element = Fire | Rock | Air | Earth | Water deriving (Eq, Show)
data ItemType = Staff | Boots | Helmet | Chestplate | Pants deriving (Eq, Show)

--Character :: (Life -> Mana -> Items -> Attack -> Ability1 -> Ability2)
data Player = Character Int Int [Item] DecideAbilityStruct AbilityStruct AbilityStruct

getAllItems :: [Element] -> [ItemType] -> [Item]
getAllItems es ts = concat (foldr(\x h -> (getAllItemsFromElem x ts) : h) [] es)

getAllItemsFromElem e ts = foldr(\x h -> (Item e x) : h) [] ts

type FightState = (Player, Player, Int)

-- fight returns all the states of a battle to death!
fight :: (Player, Player) -> [FightState]
fight (c1,c2) = figthR [(c1,c2,0)]

-- FightR returns the states of a battle to death recursively, ordered from last to first
figthR :: [FightState] -> [FightState]
figthR ((c1,c2,rounds): xs) =
  if (getLife c1 <= 0) || (getLife c2 <= 0) || (rounds > 100) then [(c1,c2,rounds+1)] else 
  let nextStage = fightOneStep (c1,c2,rounds)
  in [(c1,c2,rounds)] ++ figthR ([nextStage])

fightOneStep :: FightState -> FightState 
fightOneStep (c1,c2,rounds) =
  let (c1a,c2a) = ((getDecideAbility c1) rounds (getMana c1) (getAbilStruct1 c1) (getAbilStruct2 c1)) (c1,c2) 
  in let (c2b, c1b) = ((getDecideAbility c2a) rounds (getMana c2a) (getAbilStruct1 c2a) (getAbilStruct2 c2a)) (c2a,c1a)
  in (c1b,c2b,rounds+1)

fightAlgorithm :: Player -> Player -> Int -> Int 
fightAlgorithm c1 c2 0 = (1000 - getLife c2)
fightAlgorithm c1 c2 n = 
  let (p1, p2, r) = fightOneStep (c1,c2,0)
  in fightAlgorithm p1 p2 (n-1)

-------------------------------------------------------------
-----------------DECIDE ABILITY -----------------------------
-------------------------------------------------------------

-- DecideAbility :: turn -> availableMana -> h1 -> h2 -> selected 
data DecideAbilityStruct = DecideAbilityStruct String DecideAbility Int 
type DecideAbility = ( Int -> Int -> AbilityStruct -> AbilityStruct -> Ability )

--decideAttacksManually Always chooses the first Ability of the character
decideAttacksManuallyStruct :: DecideAbilityStruct
decideAttacksManuallyStruct = DecideAbilityStruct "Decide attacks manually" decideAttacksManually 0 

decideAttacksManually :: DecideAbility 
decideAttacksManually t am (AbilityStruct n1 h1 i1 mana1) (AbilityStruct name2 h2 i2 mana2) = nullHit


--decideFirstAbility Always chooses the first Ability of the character
decideFirstAbilityStruct :: DecideAbilityStruct
decideFirstAbilityStruct = DecideAbilityStruct "Try to use the first ability. If not enough mana, use the second." decideFirstAbility 1 

decideFirstAbility :: DecideAbility 
decideFirstAbility t am (AbilityStruct n1 h1 i1 mana1) (AbilityStruct name2 h2 i2 mana2) = if am >= mana1 then h1 else (if am >= mana2 then h2 else nullHit)

-- Always chooses the first Ability of the character
decideAlterateAbilityStruct :: DecideAbilityStruct
decideAlterateAbilityStruct = DecideAbilityStruct "Alternate between both abilities" decideAlterateAbility 2

decideAlterateAbility :: DecideAbility 
decideAlterateAbility t am (AbilityStruct n1 h1 i1 mana1) (AbilityStruct name2 h2 i2 mana2) = 
                if (mod t 2) == 0 && am >= mana1 then h1 
                else (if (mod t 2) == 1 && am >= mana2 then h2 else nullHit)

--decideForcedFirstAbility Always chooses the first Ability of the character
decideForcedFirstAbilityStruct :: DecideAbilityStruct
decideForcedFirstAbilityStruct = DecideAbilityStruct "Use first attack." decideForcedFirstAbility 3

decideForcedFirstAbility :: DecideAbility 
decideForcedFirstAbility t am (AbilityStruct n1 h1 i1 mana1) (AbilityStruct name2 h2 i2 mana2) = useAbilityOrNoHit am mana1 h1 

--decideForcedSecondAbility Always chooses the first Ability of the character
decideForcedSecondAbilityStruct :: DecideAbilityStruct
decideForcedSecondAbilityStruct = DecideAbilityStruct "Use second attack." decideForcedSecondAbility 4 

decideForcedSecondAbility :: DecideAbility 
decideForcedSecondAbility t am (AbilityStruct n1 h1 i1 mana1) (AbilityStruct name2 h2 i2 mana2) = useAbilityOrNoHit am mana2 h2 

useAbilityOrNoHit am mana h1  = if am >= mana then h1 else nullHit

-------------------------------------------------------------
------------------------- ABILITIES -------------------------
-------------------------------------------------------------

--simpleHit generates 5 damage without costing mana
simpleHitStruct :: AbilityStruct
simpleHitStruct = AbilityStruct "Single hit: Deal 5 damage" simpleHit 1 0

simpleHit :: Ability
simpleHit (c1, c2) = (c1, removeLife c2 5)

--setOnFire removes 5 + 5 * each fire tipe
setOnFireStruct :: AbilityStruct
setOnFireStruct = AbilityStruct "Set on fire: Damage is 5 x (#Fire items +1)" setOnFire 2 30

setOnFire :: Ability
setOnFire (c1, c2) = let damageAmplifier = (getElementalDamage Fire (getItems c1))+1 
        in (removeMana c1 30, removeLife c2 (5 * damageAmplifier))

--noHit does not cause damage nor cost mana
nullHitStruct :: AbilityStruct
nullHitStruct = AbilityStruct "Don't hit: Peace is always an option..." nullHit 3 0

nullHit :: Ability
nullHit (c1, c2) = (c1, c2)

--manaLeech does not cause damage nor cost mana
manaLeechStruct :: AbilityStruct
manaLeechStruct = AbilityStruct "Mana leech: Steal 10 mana points (if available). If you have at least 3 water items, steal 20." manaLeech 4 0

manaLeech :: Ability
manaLeech (c1, c2) = let waterItems = (getElementalDamage Water (getItems c1))  
                  in let manaStolen = if waterItems > 3 then 20 else 10
                  in let leechedMana =  min manaStolen (getMana c2)
                  in (removeMana c1 ((-1)*leechedMana),removeMana c2 leechedMana)

--lifeSteal 
lifeStealStruct :: AbilityStruct
lifeStealStruct = AbilityStruct "Life steal: Deal 20 damage. Recover half of the damage in life points." lifeSteal 5 30

lifeSteal :: Ability
lifeSteal (c1, c2) = let damage = min 20 (getLife c2)
                  in (removeLife c1 (div ((-1)*damage) 2),removeLife c2 damage)                  

--generateMana 

generateManaStruct :: AbilityStruct
generateManaStruct = AbilityStruct "Generate mana: Get 5 mana for each Earth item." generateMana 6 5

generateMana :: Ability
generateMana (c1, c2) = let earthItems = (getElementalDamage Earth (getItems c1))  
                  in let manaGenerated =  earthItems * 5
                  in (removeMana c1 ((-1)*manaGenerated), c2)

--powerHit
windHitStruct :: AbilityStruct
windHitStruct = AbilityStruct "Cause damage equal to the difference between your air items and the rock items of the enemy times 10." windHit 7 40

windHit :: Ability
windHit (c1, c2) = let windItems = (getElementalDamage Air (getItems c1))
                  in let rockItems = (getElementalDamage Rock (getItems c1))
                  in let damage = max 0 (windItems - rockItems)
                  in (removeMana c1 40 ,removeLife c2 (damage*10))                  

-------------------------------------------------------------
-----------------ABILITIES UTILS-----------------------------
-------------------------------------------------------------
getItemElement (Item elem itemType) = elem 

getDecideAbility :: Player -> DecideAbility
getDecideAbility (Character l m items (DecideAbilityStruct name da index) h1 h2) = da

getAbilStruct1 :: Player -> AbilityStruct
getAbilStruct1 (Character l m i da h1 h2) = h1

getAbilStruct2 :: Player -> AbilityStruct
getAbilStruct2 (Character l m i a h1 h2) = h2

getItems :: Player -> [Item]
getItems (Character l m i a h1 h2) = i

getLife :: Player -> Int
getLife (Character l m i a h1 h2) = l

getMana :: Player -> Int
getMana (Character l m i a h1 h2) = m

removeLife :: Player -> Int -> Player
removeLife (Character l m items a h1 h2) damage = (Character (l-damage) m items a h1 h2) 

removeMana :: Player -> Int -> Player
removeMana (Character l m items a h1 h2) mana = (Character l (m-mana) items a h1 h2) 

getElementalDamage :: Element -> [Item] -> Int
getElementalDamage elem = length . filter ((elem==) . getItemElement)
