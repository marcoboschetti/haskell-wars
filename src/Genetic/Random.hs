module Genetic.Random where

import Web.Scotty
import Data.Aeson (FromJSON, ToJSON)
import GHC.Generics
import Network.Wai.Middleware.Static
import System.Random

-------------------------------------------------------------
-------------------------RANDOM UTILS------------------------
-------------------------------------------------------------

rand :: (Num a,Random a) => IO a 
rand = getStdRandom (randomR (0,1))

rand1 n = getStdRandom (randomR (0,n))

random_int :: Int -> IO Int
random_int n = (getStdRandom (randomR (0,n))) :: IO Int

random_float :: Float -> IO Float
random_float n = (getStdRandom (randomR (0,n))) :: IO Float

random_prob :: IO Float
random_prob = random_float 1.0

random_pairs_int_list :: Int -> Int -> IO [(Int, Int)]
random_pairs_int_list 0 list_dim = return []
random_pairs_int_list n list_dim = do
  r1  <- random_int (list_dim -1)
  r2  <- random_int (list_dim -1)
  rs <- random_pairs_int_list (n-1) list_dim
  return ((r1, r2):rs)

random_int_list :: Int -> Int -> IO [Int]
random_int_list 0 list_dim = return []
random_int_list n list_dim = do
  r  <- random_int (list_dim -1)
  rs <- random_int_list (n-1) list_dim
  return (r:rs)

random_prob_list :: Int -> IO [Float]
random_prob_list 0 = return []
random_prob_list n = do
  r  <- random_prob
  rs <- random_prob_list (n-1)
  return (r:rs)

random_prob_nss :: Int -> Int -> IO [[Float]]
random_prob_nss 0 list_size = return []
random_prob_nss att_num list_size = do
 rs <- random_prob_list list_size
 rss <- random_prob_nss (att_num-1) list_size
 return (rs:rss)

random_int_nssss :: Int -> Int -> Int -> Int -> Int -> IO [[[[Int]]]]
random_int_nssss 0 def_abs att_num list_size list_dim = return []
random_int_nssss attack_abs def_abs att_num list_size list_dim = do
 rsss <- random_int_nsss def_abs att_num list_size list_dim
 rssss <- random_int_nssss (attack_abs-1) def_abs att_num list_size list_dim
 return (rsss:rssss)

random_int_nsss :: Int -> Int -> Int -> Int -> IO [[[Int]]]
random_int_nsss 0 att_num list_size list_dim = return []
random_int_nsss def_abs att_num list_size list_dim = do
 rss <- random_int_nss att_num list_size list_dim
 rsss <- random_int_nsss (def_abs-1) att_num list_size list_dim
 return (rss:rsss)

random_int_nss :: Int -> Int -> Int -> IO [[Int]]
random_int_nss 0 list_size list_dim = return []
random_int_nss att_num list_size list_dim = do
 rs <- random_int_list list_size list_dim
 rss <- random_int_nss (att_num-1) list_size list_dim
 return (rs:rss)