module Genetic.Algorithm where

import Web.Scotty
import Data.Aeson (FromJSON, ToJSON)
import GHC.Generics
import Network.Wai.Middleware.Static
import System.Random

import Genetic.Random
import Service.Game
import Service.Entities

-------------------------------------------------------------
---------------------------MAIN------------------------------
-------------------------------------------------------------
--getTrainedCharacter ::  Int -> Player
getTrainedCharacter n = do 
  print ("start")
  hero <- genetic_main n (getAllItems [Fire, Rock, Air, Earth, Water] [Staff, Boots, Helmet, Chestplate, Pants]) getAllAttacksRaw getAllAttacksRaw getAllDecideAbilityRaw
  print ("end")
  return hero

genetic_main :: Int -> [Item] -> [AbilityStruct] -> [AbilityStruct] -> [DecideAbilityStruct] -> IO Player
genetic_main n items has hds ats = do 
  random_int_lists <- random_int_nssss (length has) (length hds) (length ats) 5 (length items)
  let population = generate_population 100 100 items random_int_lists has hds ats
  print ("population generated")
  final_population_to_return <- genetic_algorithm population n items has hds ats
  let best_player = get_best_hero final_population_to_return
  return best_player


genetic_algorithm :: [Player] -> Int -> [Item] -> [AbilityStruct] -> [AbilityStruct] -> [DecideAbilityStruct] -> IO [Player]
genetic_algorithm ps 0 items has hds ats = return ps
genetic_algorithm ps n items has hds ats = do
 random_pairs_list <- random_pairs_int_list (length ps) (length ps)
 probs <- random_prob_list (length ps)
 let selected_population = selection ps random_pairs_list probs

 random_position_to_cross <- random_int_list (length selected_population `div` 2) 5
 let crossed_population = crossover selected_population random_position_to_cross

 mutation_probs <- random_prob_nss (length crossed_population) 5
 gen_index <- random_int_nss (length crossed_population) 5 (length items)
 ha_probs <- random_prob_list (length crossed_population)
 hd_probs <- random_prob_list (length crossed_population)
 ha_index <- random_int_list (length crossed_population) (length has) 
 hd_index <- random_int_list (length crossed_population) (length hds) 
 let mutated_population = mutation crossed_population items mutation_probs gen_index ha_probs hd_probs ha_index hd_index has hds

 pairs_to_replace <- random_pairs_int_list (length ps) (length ps + length mutated_population)
 replace_probs <- random_prob_list (length ps)
 let replaced_population = replace mutated_population ps pairs_to_replace replace_probs

 final_population <- genetic_algorithm mutated_population (n-1) items has hds ats
 
 return final_population

-------------------------------------------------------------
-----------------GENETIC ALGORITHM UTILS---------------------
-------------------------------------------------------------

generate_population :: Int -> Int -> [Item] -> [[[[Int]]]] -> [AbilityStruct] -> [AbilityStruct] -> [DecideAbilityStruct] -> [Player]
generate_population l m is [] [] h2s as = []
generate_population l m is (nsss:nssss) (h1:h1s) h2s as = (generate_population_by_avility l m is nsss h1 h2s as) ++ (generate_population l m is nssss h1s h2s as)

generate_population_by_avility :: Int -> Int -> [Item] -> [[[Int]]] -> AbilityStruct -> [AbilityStruct] -> [DecideAbilityStruct] -> [Player]
generate_population_by_avility l m is nsss h1 [] as = []
generate_population_by_avility l m is (nss:nsss) h1 (h2:h2s) as =  (generate_population_by_attack l m is nss h1 h2 as) ++ (generate_population_by_avility l m is nsss h1 h2s as)

generate_population_by_attack :: Int -> Int -> [Item] -> [[Int]] -> AbilityStruct -> AbilityStruct -> [DecideAbilityStruct] -> [Player]
generate_population_by_attack l m is nss h1 h2 [] = []
generate_population_by_attack l m is (ns:nss) h1 h2 (a:as) = ((Character l m (get_items ns is) a h1 h2):(generate_population_by_attack l m is nss h1 h2 as))

get_items :: [Int] -> [Item] -> [Item]
get_items [] is = []
get_items (n:ns) is = ((is !! n):(get_items ns is))

selection :: [Player] -> [(Int, Int)] -> [Float] -> [Player]
selection ps [] [] = []
selection ps ((x1,x2):xs) (f:fs) = ((get_winner (ps !! x1) (ps !! x2) f):(selection ps xs fs))

get_winner :: Player -> Player -> Float -> Player
get_winner p1 p2 prob = let (fitness1, fitness2) = (get_fitness p1, get_fitness p2)
 in if fitness1 > fitness2 then if prob < 0.25 then p2 else p1 else if prob < 0.25 then p1 else p2

crossover :: [Player] -> [Int] -> [Player]
crossover [] ns = []
crossover [x] ns = [x]
crossover (p1:p2:ps) (n:ns) = let (p1', p2') = cross_players p1 p2 n in (p1':p2':(crossover ps ns))   

cross_players :: Player -> Player -> Int -> (Player, Player)
cross_players (Character l1 m1 i1 a1 h11 h12) (Character l2 m2 i2 a2 h21 h22) n =
 let (i1', i2') = cross_items i1 i2 n in ((Character l1 m1 i1' a1 h11 h21), (Character l2 m2 i2' a2 h21 h12)) 

cross_items :: [Item] -> [Item] -> Int -> ([Item],[Item])
cross_items i1s i2s 0 = (i2s, i1s)
cross_items (i1:i1s) (i2:i2s) n = let (i1s', i2s') = cross_items i1s i2s (n-1) in ((i1:i1s'), (i2:i2s'))   

mutation :: [Player] -> [Item] -> [[Float]] -> [[Int]] -> [Float] -> [Float] -> [Int] -> [Int] -> [AbilityStruct] -> [AbilityStruct] ->[Player]
mutation [] is [] [] [] [] [] [] has hds = []
mutation (p:ps) is (fs:fss) (ns:nss) (fa:fas) (fd:fds) (ia:ias) (id:ids) has hds =
 ((mutation_by_player p is fs ns fa fd ia id has hds):(mutation ps is fss nss fas fds ias ids has hds))

mutation_by_player :: Player -> [Item] -> [Float] -> [Int] -> Float -> Float -> Int -> Int -> [AbilityStruct] -> [AbilityStruct] -> Player
mutation_by_player (Character l m i a h1 h2) items fs ns f1 f2 i1 i2 has hds = 
 let (h1', h2') = mutation_by_hability h1 h2 f1 f2 i1 i2 has hds
 in (Character l m (mutation_by_item i items fs ns) a h1' h2')

mutation_by_item :: [Item] -> [Item] -> [Float] ->[Int] -> [Item]
mutation_by_item [] is [] [] = []
mutation_by_item (i:is') is (f:fs) (n:ns) = if f < 0.005 then ((is !! n):(mutation_by_item is' is fs ns)) else (i:(mutation_by_item is' is fs ns)) 

mutation_by_hability :: AbilityStruct -> AbilityStruct -> Float -> Float -> Int -> Int -> [AbilityStruct] -> [AbilityStruct] ->(AbilityStruct, AbilityStruct)
mutation_by_hability h1 h2 f1 f2 i1 i2 has hds = (if f1 < 0.005 then has !! i1 else h1, if f2 < 0.005 then hds !! i2 else h2 ) 

replace :: [Player] -> [Player] -> [(Int, Int)] -> [Float] -> [Player]
replace ps1 ps2 nss fs = selection (ps1 ++ ps2) nss fs

get_fitness :: Player -> Int
get_fitness p1 = fightAlgorithm p1 (Character 1000 1000 [] decideFirstAbilityStruct nullHitStruct nullHitStruct) 15

get_best_hero :: [Player] -> Player
get_best_hero ps = ps!!(get_best_player ps 0 0 0)

get_best_player :: [Player] -> Int -> Int -> Int -> Int
get_best_player [] best_fitness best_f_index current_index = best_f_index
get_best_player (p:ps) best_fitness best_f_index current_index =
 let fitness = get_fitness p in
 if (fitness > best_fitness) then get_best_player ps fitness current_index (current_index + 1)
 else get_best_player ps best_fitness best_f_index (current_index + 1)