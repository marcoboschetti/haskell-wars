var inventory = [];
var inventory2 = [];
var loadedDecideAbilities = []; 
var loadedAbilities = []; 

var currentBattle = []; 

var decideForcedFirstAbility  = 3
var decideForcedSecondAbility  = 4
var slider = {};
$( document ).ready(function() {
	slider = document.getElementById("myRange");
	var output = document.getElementById("demo");
	output.innerHTML = slider.value;

	slider.oninput = function() {
		output.innerHTML = this.value;
	}


	$("input[name='enemyMode']").change(function(){
		if($(this).val() == "human"){
			$("#enemyBuilder").attr('hidden', false);
			$("#enemyBuilderSlider").attr('hidden', true);
		}else{
			$("#enemyBuilder").attr('hidden', true);
			$("#enemyBuilderSlider").attr('hidden', false);
		}
	});

	$.get( '/decideAbilities').done(function(decideAbilities){
		loadedDecideAbilities = decideAbilities;
		var decideAbilitiesSelectHTML = '';
		var decideAbilitiesSelectHTML2 = '';
		for(var i = 0; i < decideAbilities.length; i++){
			decideAbilitiesSelectHTML += '<option value="'+decideAbilities[i].daid+'">'+decideAbilities[i].daname+'</option>'
			if(i > 0){
				decideAbilitiesSelectHTML2 += '<option value="'+decideAbilities[i].daid+'">'+decideAbilities[i].daname+'</option>'
			}
		}
		$("#decideAbilitiesContainer").html(decideAbilitiesSelectHTML);
		$("#decideAbilitiesContainer2").html(decideAbilitiesSelectHTML2);
	})

	$.get( '/abilities').done(function(abilities){
		loadedAbilities = abilities;

		var attackSelectHTML = "";
		for(var i = 0; i < abilities.length; i++){
			attackSelectHTML += '<option value="'+abilities[i].aid+'">'+abilities[i].aname+' (Mana: '+abilities[i].amana+')</option>'
		}
		$("#attack1container").html(attackSelectHTML);
		$("#attack2container").html(attackSelectHTML);
		$("#attack1container2").html(attackSelectHTML);
		$("#attack2container2").html(attackSelectHTML);
	})

	$("#decideAbilitiesContainer").change(function(){
		if($(this).val() == 0){
			$("#manualStrategyDesc").show();
		}else{
			$("#manualStrategyDesc").hide();
		}
	});
});

function addItem(index){
	if( index == 1){
		var element = $("#newItemElement").val();
		var itemType = $("#newItemType").val();
		inventory.push({element: element, itemType: itemType});
		renderInventory(1,inventory,"#itemRenderContainer", "#itemLabelContainer","#itemRenderContainer","#itemLabelContainer");
		if (inventory.length == 5){
			$("#addItemBtn").attr('disabled', true);
		}
	}else{
		var element = $("#newItemElement2").val();
		var itemType = $("#newItemType2").val();
		inventory2.push({element: element, itemType: itemType});
		renderInventory(2,inventory2,"#itemRenderContainer2", "#itemLabelContainer2","#itemRenderContainer2","#itemLabelContainer2");
		if (inventory2.length == 5){
			$("#addItemBtn2").attr('disabled', true);
		}
	}
}

function renderInventory(index,l,itemRenderContainer, itemLabelContainer,itemRenderContainer,itemLabelContainer){
	$(itemRenderContainer).empty();
	$(itemLabelContainer).empty();
	var itemsHtml = "";
	var labelsHtml = "";
	for(var i = 0; i < l.length; i++){
		var item = l[i];
		itemsHtml += '<div class="col-md-4" style="padding-bottom:1em;"><img class="item-sprite" src="./sprites/'+item.itemType+'/'+item.element+'.png"/></div>';
		labelsHtml +='<span class="item-label"><h2><span class="label label-default">'+item.element+ ' '+item.itemType;
		labelsHtml +='<span class="glyphicon glyphicon-remove" onclick="removeItem('+index+',\''+item.element+'\',\''+item.itemType+'\');"aria-hidden="true"></span></h2></span>';
	}
	$(itemRenderContainer).html(itemsHtml);
	$(itemLabelContainer).html(labelsHtml);
}

function removeItem(index, element, itemType){
	if(index == 1){
		for(var i = 0; i < inventory.length; i++){
			var item = inventory[i];
			if(item.element = element && item.itemType == itemType){
				inventory.splice(i,1);
				renderInventory(1,inventory,"#itemRenderContainer", "#itemLabelContainer","#itemRenderContainer","#itemLabelContainer");
				$("#addItemBtn").attr('disabled', false);
				return;
			}
		}
	}else{
		for(var i = 0; i < inventory2.length; i++){
			var item = inventory2[i];
			if(item.element = element && item.itemType == itemType){
				inventory2.splice(i,1);
				renderInventory(2,inventory2,"#itemRenderContainer2", "#itemLabelContainer2","#itemRenderContainer2","#itemLabelContainer2");
				$("#addItemBtn2").attr('disabled', false);
				return;
			}
		}
	}
}

function battleToDeath(){
	var character1 = {};
	character1.ability1Index = parseInt($("#attack1container").val());
	character1.ability2Index = parseInt($("#attack2container").val());
	character1.decideAbilityIndex = parseInt($("#decideAbilitiesContainer").val());
	character1.items = inventory;
	character1.life = 100;
	character1.mana = 100;


	var character2 = {};
	character2.ability1Index = parseInt($("#attack1container2").val());
	character2.ability2Index = parseInt($("#attack2container2").val());
	character2.decideAbilityIndex = parseInt($("#decideAbilitiesContainer2").val());
	character2.items = inventory2;
	character2.mana = 100;

	var generations = parseInt(slider.value);
	var enemyMode = $("input[name='enemyMode']:checked").val();
	if(enemyMode == "human"){
		character2.life = 100;
	}else{
		character2.life = -1;
	}

	battleState = {player1 : character1, player2 : character2, round: generations};

	$("#selectCharacterPanel").attr('hidden', true);
	$("#selectCharacterPanel2").attr('hidden', true);
	$("#trainingRivalPanel").attr('hidden', false);

	if(character1.decideAbilityIndex == 0){
		postStartManualBattle(battleState);
	}else{
		postBattleToFight(battleState);
	}
}

function postBattleToFight(battleState){
	$.ajax({
		url : '/fightToDeath',
		type: "post",
		headers: {'Content-Type': 'application/json'},
		data: JSON.stringify(battleState),
		success: function( data ) {
			currentBattle = data;
			renderBattle();
		}
	});
}

function postStartManualBattle(battleState){
	$.ajax({
		url : '/initFight',
		type: "post",
		headers: {'Content-Type': 'application/json'},
		data: JSON.stringify(battleState),
		success: function( data ) {
			currentBattle = [data];
			renderBattle();
		}
	});
}

var currentRenderedStage = 0;

function renderBattle(){
	var finalState = currentBattle[currentBattle.length-1];
	
	var battleResult = getBattleResult(finalState);
	$("#battleResultContainer").text(battleResult);

	var renderIndex = 0
	var finalState = currentBattle[currentBattle.length-1];
	if(finalState.player1.decideAbilityIndex == 0){
		renderIndex = currentBattle.length-1;
	}
	renderBattleStage(renderIndex);

	$("#trainingRivalPanel").attr('hidden', true);
	$("#battleRendererPanel").attr('hidden', false);
}

function postBattleNextStage(player1DecideAbility){
	currentStage = currentBattle[currentBattle.length-1];
	currentStage.player1.decideAbilityIndex = player1DecideAbility;
	$.ajax({
		url : '/fightNextStage',
		type: "post",
		headers: {'Content-Type': 'application/json'},
		data: JSON.stringify(currentStage),
		success: function( data ) {
			// We changed the posted player decide ability, correcting it
			data.player1.decideAbilityIndex = 0;
			currentBattle.push(data);
			renderBattle();
		}
	});
}

function getBattleResult(finalState){
	var battleResult = "tied";
	if(finalState.player1.life > 0 &&  finalState.player2.life > 0){
		battleResult = "are still fighting...";
	} else if(finalState.player1.life > finalState.player2.life){
		battleResult = "Won! =)";
	}else if (finalState.player1.life < finalState.player2.life){
		battleResult = "Lost. Go get revenge...";
	}
	return battleResult;
}

function moveRelativeState(rel){
	renderBattleStage(currentRenderedStage+rel);
}

function renderBattleStage(stageIndex){
// Stuff related with footer state selector
currentRenderedStage = stageIndex;
if(currentRenderedStage <= 0){
	$("#prevStageBtn").attr('disabled', true);
	currentRenderedStage = 0;
}else{
	$("#prevStageBtn").attr('disabled', false);
}
if(currentRenderedStage >= currentBattle.length-1){
	$("#nextStageBtn").attr('disabled', true);
	currentRenderedStage = currentBattle.length-1;
}else{
	$("#nextStageBtn").attr('disabled', false);
}
var battleStagesSelector = "";
for(var i = 0; i < currentBattle.length; i++){
	var selectedClass= "";
	if(i == stageIndex){
		selectedClass = "stageBtnSelected";
	}
	battleStagesSelector += '<a href="#" class="stageBtn" onclick="renderBattleStage('+i+');return false;"><span  class="label label-info '+selectedClass+'">'+i+'</span></a>';
}
$("#battleRendererPanelFooter").html(battleStagesSelector);

var finalState = currentBattle[currentBattle.length-1];

var canDecideAttack = false;
if(finalState.player1.life > 0 &&  finalState.player2.life > 0){
	$("#resetFightBtn").hide();
	canDecideAttack = (stageIndex == currentBattle.length-1);
}else{
	$("#resetFightBtn").show();
	var battleResult = getBattleResult(finalState);
	$("#battleResultContainer").text(battleResult);
}

var stage = currentBattle[currentRenderedStage];
$("#battleRendererLeft").html(getPlayerRenderSide(stage.player1, canDecideAttack));
$("#battleRendererRight").html(getPlayerRenderSide(stage.player2, false));

}


function getPlayerRenderSide(player, canDecideAttack){
	var playerStateHTML ="";
	playerStateHTML += '<div class"row">';
	playerStateHTML += '<div class="col-md-1"></div>';
	playerStateHTML += '<div class="col-md-5"><h2>Life:<strong>'+player.life+'</strong></h2></div>';
	playerStateHTML += '<div class="col-md-5"><h2>Mana:<strong>'+player.mana+'</strong></h2></div>';
	playerStateHTML += '</div>';

	var decideAtk1Btn = "";
	var decideAtk2Btn = "";
	if (canDecideAttack){
		decideAtk1Btn = '<button type="button" class="btn btn-success" id="resetFightBtn" onclick="postBattleNextStage('+decideForcedFirstAbility+');"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></button>';
		decideAtk2Btn = '<button type="button" class="btn btn-success" id="resetFightBtn" onclick="postBattleNextStage('+decideForcedSecondAbility+');"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></button>';
	}

	playerStateHTML += '<div class"row"><div class="col-md-12">Strategy:<strong>'+getDecideAbilityName(player.decideAbilityIndex)+'</strong></div></div>';
	playerStateHTML += '<div class"row">';
	playerStateHTML += '</div><div class="col-md-6">Attack 1:<strong>'+getAbilityName(player.ability1Index)+'</strong>'+decideAtk1Btn+'</div>';
	playerStateHTML += '</div><div class="col-md-6">Attack 2:<strong>'+getAbilityName(player.ability2Index)+'</strong>'+decideAtk2Btn+'</div>';
	playerStateHTML += '</div>';


	playerStateHTML += '<div><h3>Inventory</h3><div>';
	for(var i = 0; i < player.items.length; i++){
		var item = player.items[i];
		playerStateHTML += '<div class="col-md-4" style="padding-bottom:1em;"><img class="item-sprite" src="./sprites/'+item.itemType+'/'+item.element+'.png"/></div>';
	}

	playerStateHTML += '</div></div>';
	return playerStateHTML
}

function getDecideAbilityName(index){
	for(var i = 0; i < loadedDecideAbilities.length; i++){
		if(loadedDecideAbilities[i].daid == index){
			return loadedDecideAbilities[i].daname;
		}
	}
}

function getAbilityName(index){
	for(var i = 0; i < loadedAbilities.length; i++){
		if(loadedAbilities[i].aid == index){
			return loadedAbilities[i].aname;
		}
	}
}

function resetApp(){
	$("#selectCharacterPanel").attr('hidden', false);
	$("#selectCharacterPanel2").attr('hidden', false);
	$("#battleRendererPanel").attr('hidden',  true);
}