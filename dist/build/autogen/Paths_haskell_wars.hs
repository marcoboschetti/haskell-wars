module Paths_haskell_wars (
    version,
    getBinDir, getLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/tritoon/.cabal/bin"
libdir     = "/home/tritoon/.cabal/lib/x86_64-linux-ghc-7.10.3/haskell-wars-0.1.0.0-4U3nElIaG5b2XNR3wsRi0x"
datadir    = "/home/tritoon/.cabal/share/x86_64-linux-ghc-7.10.3/haskell-wars-0.1.0.0"
libexecdir = "/home/tritoon/.cabal/libexec"
sysconfdir = "/home/tritoon/.cabal/etc"

getBinDir, getLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "haskell_wars_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "haskell_wars_libdir") (\_ -> return libdir)
getDataDir = catchIO (getEnv "haskell_wars_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "haskell_wars_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "haskell_wars_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
